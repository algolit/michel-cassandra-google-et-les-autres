import models
from sqlalchemy import select, func, desc
from settings import MAX_SENTENCE_LENGTH, LANGUAGE
from db import db_session
import csv


stmt = select(models.Keyword, func.count(models.Sentence.id).label("sentence_count"))\
  .join(models.Keyword, models.Sentence.keywords)\
  .join(models.Rhyme)\
  .group_by(models.Keyword.id)\
  .where(models.Sentence.word_count < MAX_SENTENCE_LENGTH, models.Rhyme.rhyme != '')\
  .order_by(desc('sentence_count'))
  # .having(func.count(models.Sentence.id) >= 750)\
  #.order_by(desc('sentence_count'))

with open('eligible-keywords-{}.csv'.format(LANGUAGE), 'w') as h:
  writer = csv.writer(h)
  writer.writerow(['keyword', 'eligible poem sentences', 'articles', 'year added'])
  for keyword, sentence_count in db_session.execute(stmt):
    writer.writerow([keyword.keyword, sentence_count, len(keyword.articles), keyword.articles[0].date.year])
    
