from parsing_etraces import parseAll

parsed = parseAll('../data')

count_total = 0

for lang, years in parsed.items():
  count_lang = 0

  print('-------------------')
  print(lang)

  for year in sorted(years.keys()):
    print(year, len(years[year]))
    count_lang += len(years[year])

  print('Total:', count_lang)
  count_total += count_lang

print('')
print('---------------------')
print('Grand total', count_total)