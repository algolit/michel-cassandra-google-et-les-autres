from parsing_etraces import parseAll
from collections import Counter
import datetime

print('Parsing data...')

parsed = parseAll('../data')

counter = Counter()
article_counter = 0

for lang, years in parsed.items():
  for articles in years.values():
    article_counter += len(articles)
    for article in articles:
      if not article['date']:
        print(article)

    counter.update([datetime.date(year=article['date'].year, month=article['date'].month, day=1) for article in articles])

for key in sorted(counter.keys()):
  print('{}/{}'.format(key.month, key.year), counter[key])

print(sum(counter.values()))
print(article_counter)

# print('--------------------')
# for year in sorted(count.keys()):
#   print('{year}: {yearcount}'.format(year=year, yearcount=count[year]))
