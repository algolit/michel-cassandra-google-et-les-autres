from flask import Flask, render_template, jsonify
from sqlalchemy import select, func, bindparam
from db import db_session
from poem import make_poem
from utils import make_abs_path
import models
import random

from settings import BASEURL, MAX_SENTENCE_LENGTH, MIN_SENTENCES

app = Flask(__name__)

@app.teardown_appcontext
def shutdown_session(exception=None):
  db_session.remove()


languages = db_session.execute(select(models.Language)).scalars().all()

eligible_keyword_query = select(models.Keyword)\
  .join(models.Keyword, models.Sentence.keywords)\
  .join(models.Rhyme)\
  .group_by(models.Keyword.id)\
  .where(
    models.Keyword.language_id == bindparam('language_id'),
    models.Sentence.word_count < MAX_SENTENCE_LENGTH,
    models.Rhyme.rhyme != ''
  )\
  .having(func.count(models.Sentence.id) >= MIN_SENTENCES)

eligible_keywords = {
  language.language: list(db_session.execute(
    eligible_keyword_query,
    { 'language_id': language.id}
  ).scalars()) for language in languages
}

@app.route('{}/'.format(BASEURL))
def index ():
  return render_template('index.html', BASEURL=BASEURL)


"""
  Installation view of the project
"""
@app.route('{}/installation'.format(BASEURL))
def installation ():
  language = random.choice(languages)
  keyword = random.choice(eligible_keywords[language.language])

  lines = make_poem(db_session, keyword)

  if lines:
    return render_template('installation.html', lines=lines, keyword=keyword, language=language.language)
  else:
    return render_template('installation.html', lines=[], language=language.language)


@app.route('{}/poem'.format(BASEURL))
def poem ():
  language = random.choice(languages)
  keyword = random.choice(eligible_keywords[language.language])

  lines = make_poem(db_session, keyword)

  if lines:
    return render_template('poem.html', lines=lines, keyword=keyword, language=language.language)
  else:
    return render_template('poem.html', lines=[], keyword=keyword, language=language.language)


@app.route('{}/new_lines'.format(BASEURL))
def new_lines ():
  language = random.choice(languages)
  keyword = random.choice(eligible_keywords[language.language])

  lines = make_poem(db_session, keyword)

  if not lines:
    lines = []

  return jsonify({
    'keyword': keyword.keyword,
    'language': language.language,
    'lines': [{ 
      'line': line.text,
      'article': {
        'title': line.article.title,
        'url': line.article.url,
        'date': line.article.date.strftime('%d-%m-%Y'),
        'source': line.article.source.name
      }
    } for line in lines]
  })