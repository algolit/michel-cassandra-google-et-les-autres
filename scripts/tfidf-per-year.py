# from pattern.fr import parsetree
from settings import ETRACES_DATA_FOLDER
from pattern.vector import LEMMA, Document, Model, TFIDF
from parsing_etraces import parseAll, getAllArticles
from collections import Counter
from controlcodes import ERASE_LINE, RESET_CURSOR
import sys

if __name__ == '__main__':
  data = parseAll(ETRACES_DATA_FOLDER)
  print('Importing data')
  # articles = getAllArticles(data, 'en')
  for year in sorted(data['en'].keys()):
    print(year)
    documents = []
    keyword_counter = Counter()
    # print('Loading documents')

    articles = data['en'][year]

    for k, article in enumerate(articles):
      # if k % 10 == 0:
      #   sys.stdout.write(RESET_CURSOR + ERASE_LINE)
      #   sys.stdout.write('{} / {}'.format(k, len(articles)))
      #   sys.stdout.flush()
        
      raw_text = article['title'] + '\n\n' + article['text']
      # Load as a document, remove stopwords, lemmatize
      document = Document(raw_text, name=article['id'], stopwords=False)
      documents.append(document)

    # sys.stdout.write(RESET_CURSOR + ERASE_LINE)
    # sys.stdout.flush()

    # print("Constructing model")

    model = Model(documents=documents, weight=TFIDF)

    # print('Calculating keywords')
    for k, document in enumerate(model.documents):
      # if k % 10 == 0:
      #   sys.stdout.write(RESET_CURSOR + ERASE_LINE)
      #   sys.stdout.write('{} / {}'.format(k, len(articles)))
      #   sys.stdout.flush()
      articles[k]['calculated_keywords'] = document.keywords(5)
      keyword_counter.update([keyword for (_, keyword) in articles[k]['calculated_keywords']])


    # sys.stdout.write(RESET_CURSOR + ERASE_LINE)
    # sys.stdout.flush()

    print(keyword_counter.most_common(50))