from parsing_etraces import parseAll

print('Parsing data...')

parsed = parseAll('../data')

count = {}

for lang, years in parsed.items():
  for year in sorted(years.keys()):
    if year not in count:
      count[year] = 0

    
    count[year] += len(years[year])

print('--------------------')
for year in sorted(count.keys()):
  print('{year}: {yearcount}'.format(year=year, yearcount=count[year]))