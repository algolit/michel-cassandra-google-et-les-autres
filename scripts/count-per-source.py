from parsing_etraces import parseAll
from collections import Counter
import datetime
from utils import normalize_source_url

print('Parsing data...')

parsed = parseAll('../data')

source_counter_overall = Counter()
source_counters_year = {}
article_counter = 0


for lang, years in parsed.items():
  for year, articles in years.items():
    article_counter += len(articles)
    sources = [normalize_source_url(article['source']) for article in articles if article['source']]

    if year not in source_counters_year:
      source_counters_year[year] = Counter()

    source_counters_year[year].update(sources)
    source_counter_overall.update(sources)


for year in sorted(source_counters_year.keys()):
  print('\n\n\n---')
  print(year)
  print('---')

  for source, count in source_counters_year[year].most_common():
    print(source, count)

print('---')
for source, count in source_counter_overall.most_common():
  print(source, count)
print('---')
print('Articles with sources')
print(sum(source_counter_overall.values()))


most_common_sources = source_counter_overall.most_common(15)
most_common_sources_per_year = []

for source, count in most_common_sources:  
  line = [source]

  for year in sorted(source_counters_year.keys()):
    if source in source_counters_year[year]:
      line.append(str(source_counters_year[year][source]))  
    else:
      line.append(str(0))

  print(' '.join(line))
