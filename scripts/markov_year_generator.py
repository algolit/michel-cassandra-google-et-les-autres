import json
from markov import Markov, START_TOKEN
import random
import re
import os.path

keyword = 'biometrics'
language = 'en'

data = json.load(open(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data', '{}.json'.format(language))))

if language == 'fr':
	from pattern.fr import parsetree
elif language == 'en':
	from pattern.en import parsetree
else:
	from pattern.nl import parsetree

corpus = []
models = []

"""
{
	'fr': {
		'2007': [
			{
				'id': 'id',
				'title': 'title',
				'date': 'date',
				'keywords': {
					'company': [],
					'web': [],
					'tools': [],
					'subject': [],
					'citizen_protection': [],
				},
				'zone': [],
				'type': []
				'text': 'text',
				'source': (title, url)
			},
		]
	},
}
"""

def has_keyword_filter (article):
  return keyword in article['keywords']['subject']

for year in sorted(data[language].keys()):
	print(year)
	if year in data[language]:
		corpus.append(list(filter(has_keyword_filter, data[language][year])))

		if len(corpus[-1]) > 0:
			markov_generator = Markov(window=2)

			random.shuffle(corpus[-1])

			for article in corpus[-1][:100]:
				parsed_text = parsetree(article['text'], tokenize=True, tags=False, chunks=False, relations=False, lemmata=False)
				# tokens = [[word.string for word in sentence.words] for sentence in parsed_text.sentences]
				tokens = []

				for sentence in parsed_text.sentences:
					tokens.extend([START_TOKEN] + [word.string for word in sentence.words])

				markov_generator.read(tokens)

			models.append(markov_generator)
		else:
			models.append(None)
	else:
		models.append(None)

def make_text (words):
  text = ''
  first_word = True
  captilize = True

  for word in words:
    if re.match(r'\W', word) is not None:
      text += word

      if re.match(r'[!?\.]+$', word) is not None:
        captilize = True

      if re.match(r'\n+$', word) is not None:
        first_word = True

    else:
      if not first_word:
        text += ' '

      text += word.capitalize() if captilize else word
      captilize = False
      first_word = False

  return text


for model in models:
	if model:
		line = make_text(model.generate(50, start=START_TOKEN, stop=lambda w: (w == '.' or w == '?' or w == '!')))
		print(line)
	else:
		print('***')
	print('')

# print("Reading text file", flush=True)
# with open('input/cat.txt', 'r') as h:
#   # Read text file
#   raw_text = h.read()

#   print("Tokenizing", flush=True)
  

#   print("\n*****")
#   print("Making model (bigram)", flush=True)
#   model = make_markov_model(tokens, history = 2)
#   print(get_model_statistics(model))
  
#   print("Generating text", flush=True)
#   print(make_text(generate(model, seed=["the", "cat"])))
