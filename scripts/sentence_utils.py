import models
from sqlalchemy import select, func

def find_rhyme (session, rhyme):
  return session.execute(
    select(models.Rhyme)
      .where(models.Rhyme.rhyme == rhyme)
      .limit(1)
  ).scalar()

def find_language(session, language):
  return session.execute(select(models.Language).where(models.Language.language == language).limit(1)).scalar()

def find_keyword(session, keyword):
  return session.execute(select(models.Keyword).where(models.Keyword.keyword_lower == keyword.lower()).order_by(func.random()).limit(1)).scalar()

def random_keyword (session):
  return session.execute(select(models.Keyword).order_by(func.random()).limit(1)).scalar()

def find_sentence(session, keyword=None, word_count=None, syllable_count=None, rhyme=None, language=None, last_word=None):
  filters = []

  if word_count:
    filters.append(models.Sentence.word_count == word_count)

  if keyword:
    filters.append(models.Sentence.keywords.contains(keyword))

  if syllable_count:
    filters.append(models.Sentence.syllable_count == syllable_count)

  if language:
    filters.append(models.Sentence.language_id == language.id)

  if rhyme:
    filters.append(models.Rhyme.rhyme == rhyme)
  
  if last_word:
    filters.append(models.Sentence.last_word == last_word)

  stmt = select(models.Sentence).join(models.Rhyme).where(*filters).order_by(func.random()).limit(1)

  return session.execute(stmt).scalar()
