from sqlalchemy.orm import declarative_base, relationship, Session
from sqlalchemy import Column, Integer, String, ForeignKey, Table, Date
from db import Base

#
# Models used for the database based poem generation.
#

class Language(Base):
  __tablename__ = 'language'

  id = Column(Integer, primary_key=True, autoincrement=True)
  language = Column(String)
  

# source
# - name: string
# - url: string
class Source(Base):
  __tablename__ = 'source'

  id = Column(Integer, primary_key=True, autoincrement=True)
  name = Column(String)
  url = Column(String)
  language_id = Column(Integer, ForeignKey('language.id'), index=True)

  language = relationship('Language')

# article_keyword
# - article_id: int
# - keyword_id: int
article_keyword = Table('article_keyword', Base.metadata,
    Column('article_id', ForeignKey('article.id')),
    Column('keyword_id', ForeignKey('keyword.id'), index=True)
)

# keyword_category
# - id: int
# - category: string
class KeywordCategory(Base):
  __tablename__ = 'keyword_category'

  id = Column(Integer, primary_key=True, autoincrement=True)
  category = Column(String)
  language_id = Column(Integer, ForeignKey('language.id'), index=True)

  language = relationship('Language')

# keyword
# - id: int
# - category_id: int
# - keyword: string
class Keyword(Base):
  __tablename__ = 'keyword'

  id = Column(Integer, primary_key=True, autoincrement=True)
  keyword = Column(String)
  keyword_lower = Column(String)
  category_id = Column(Integer, ForeignKey('keyword_category.id'))
  language_id = Column(Integer, ForeignKey('language.id'), index=True)

  category = relationship('KeywordCategory', backref='keywords')
  articles = relationship('Article', back_populates='keywords', secondary=article_keyword, order_by='Article.date')
  language = relationship('Language')

# article
# - title: string
# - source: int
# - url: string
# - text: string
class Article(Base):
  __tablename__ = 'article'
  id = Column(Integer, primary_key=True, autoincrement=True)

  title = Column(String)
  text = Column(String)
  url = Column(String)
  source_id = Column(Integer, ForeignKey('source.id'))
  year = Column(Integer)
  date = Column(Date)
  language_id = Column(Integer, ForeignKey('language.id'), index=True)

  language = relationship('Language')
  source = relationship('Source', backref='articles')
  keywords = relationship('Keyword', secondary=article_keyword, back_populates='articles')


# sentence_keyword
# - sentence_id: int
# - keyword: int
sentence_keyword = Table('sentence_keyword', Base.metadata,
    Column('sentence_id', ForeignKey('sentence.id')),
    Column('keyword_id', ForeignKey('keyword.id'))
)

from sqlalchemy import Index

index_sentence_keyword = Index('index_sentence_keyword', sentence_keyword.c.sentence_id, sentence_keyword.c.keyword_id)
index_keyword_sentence = Index('index_keyword_sentence', sentence_keyword.c.keyword_id, sentence_keyword.c.sentence_id)


# sentence
# - id: int
# - article_id: int
# - order: int
# - text: string
# - syllable_count: int
# - last_word: string
# - last_word_rhyme: int
class Sentence(Base):
  __tablename__ = 'sentence'

  id = Column(Integer, primary_key=True, autoincrement=True)
  article_id = Column(Integer, ForeignKey('article.id'), index=True)
  text = Column(String)
  word_count = Column(Integer, index=True)
  syllable_count = Column(Integer, index=True)
  last_word = Column(String)
  last_word_rhyme_id = Column(Integer, ForeignKey('rhyme.id'), index=True)
  language_id = Column(Integer, ForeignKey('language.id'), index=True)

  language = relationship('Language')
  article = relationship('Article', backref='sentences', order_by='Sentence.id')
  last_word_rhyme = relationship('Rhyme', backref='sentences')
  keywords = relationship('Keyword', backref='sentences', secondary=sentence_keyword)


# rhymes
# - id: int
# - rhyme: string
class Rhyme(Base):
  __tablename__ = 'rhyme'

  id = Column(Integer, primary_key=True, autoincrement=True)
  rhyme = Column(String)
  language_id = Column(Integer, ForeignKey('language.id'), index=True)

  language = relationship('Language')