from collections import UserList
from urllib.parse import urlsplit
import json
import os.path
import re

"""
  Returns a normalized source.
  Expects a source tuple as input.

"""
def normalize_source_url (source):
  title, url = source
  urlparts = urlsplit(url)

  netloc = urlparts[1]

  netlocparts = netloc.split('.')

  if netlocparts[-2:] == ['co', 'uk']:
    return '.'.join(netlocparts[-3:])

  return '.'.join(netlocparts[-2:])

def make_abs_path(*parts):
  return os.path.join(os.path.dirname(os.path.abspath(__file__)), *parts)

def load_data (language):
  return json.load(open(make_abs_path('data', '{}.json'.format(language)), 'r'))


def get_last_word (sentence):
  pos = -1

  while re.match('\W', sentence.words[pos].string) and abs(pos) < len(sentence.words) - 1:
    pos -= 1

  return sentence.words[pos].string