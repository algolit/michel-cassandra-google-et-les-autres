from re import L
import models
from sqlalchemy import select, func, and_
from settings import MAX_SENTENCE_LENGTH

"""
  Tries to find pair of rhyming sentences for given keyword.
  Will try multiple times, amount of maximum attempts is 
  controlled through the attempts variable.

  session:Session sqlalchemy session
  keyword:Keyword Keyword model
  attempts:integer amount of times the select is attempted
  tried: a list of sentences that where tried

  returns None | (Sentence, Sentence)
"""
def find_rhyming_pair (session, keyword, attempts=50, tried=[]):
  # select a first random sentence, which
  # is linked to the given keyword
  # print('first')
  sentence = session.execute(
    select(models.Sentence)
      .join(models.Rhyme)
      .where(
        models.Sentence.id not in (tried),
        models.Sentence.word_count < MAX_SENTENCE_LENGTH,
        models.Rhyme.rhyme != '',
        models.Sentence.keywords.contains(keyword)
      )
      .order_by(func.random())
      .limit(1)
    ).scalar()

  # select random rhyming sentence, which is not the first sentence
  # but has same rhyme and syllable count

  # print('rhyme')
  rhyming_sentence = session.execute(
    select(models.Sentence)
      .where(
        models.Sentence.id != sentence.id,
        models.Sentence.text != sentence.text,
        models.Sentence.last_word != sentence.last_word,
        models.Sentence.syllable_count == sentence.syllable_count,
        models.Sentence.word_count < MAX_SENTENCE_LENGTH,
        models.Sentence.last_word_rhyme_id == sentence.last_word_rhyme_id,
        models.Sentence.keywords.contains(keyword)
      )
      .order_by(func.random())
      .limit(1)
    ).scalar()


  # if not rhyming_sentence:
  #   print('rhyme alt')
  #   rhyming_sentence = session.execute(
  #     select(models.Sentence)
  #     .where(
  #       models.Sentence.id != sentence.id,
  #       models.Sentence.text != sentence.text,
  #       models.Sentence.last_word != sentence.last_word,
  #       models.Sentence.word_count < 13,
  #       and_(models.Sentence.syllable_count >= sentence.syllable_count-2, models.Sentence.syllable_count <= sentence.syllable_count+2),
  #       models.Sentence.last_word_rhyme_id == sentence.last_word_rhyme_id,
  #       models.Sentence.keywords.contains(keyword)
  #     )
  #     .order_by(func.random())
  #     .limit(1)
  #   ).scalar()

  if sentence and rhyming_sentence:
    return (sentence, rhyming_sentence)
  else:
    # If no rhyming sentence was found, continue search
    # if no start sentence was found stop search
    if sentence and attempts > 0:
      tried.append(sentence.id)
      return find_rhyming_pair(session, keyword, attempts-1, tried)
    else:
      return None

"""
  Generate a poem for given keyword.
  A poem consists of 4 lines

  a1 b1 b2 a2
"""
def make_poem (session, keyword):
  a = find_rhyming_pair(session, keyword)

  if a:
    b = find_rhyming_pair(session, keyword, tried=[a[0].id, a[1].id])

    if a and b:
      return (a[0], b[0], b[1], a[1])
    else:
      return None
  else:
    return None