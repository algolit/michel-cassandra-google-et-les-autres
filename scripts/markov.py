import random
import math
import copy

START_TOKEN = '__start__'
END_TOKEN = '__end__'


class Markov (object):
  def __init__ (self, window=3):
    self.keys = []
    # { (word1, .., wordn-1): wordn, }
    self.data = {}
    self.window = window

  def as_key (self, word):
    return word.lower()

  def make_key (self, words):
    return tuple([self.as_key(w) for w in words])

  # start tuple (w1, w2, ...)
  def key_starts_with (self, start):
    start = tuple(map(self.as_key, start))
    def filter (key):
      if key[0:len(start)] == start:
        return True
      
      return False

    return filter

  # Tokens is a list of strings
  def read (self, tokens):
    for i in range(self.window, len(tokens)):
      key = self.make_key(tokens[(i-self.window):i])
      if not key in self.data:
        self.keys.append(key)
        self.data[key] = []
      
      self.data[key].append(tokens[i])

  def generate (self, length = 50, seed = None, start = None, stop = None):
    if seed is None:
      if start:
        if type(start) != tuple:
          start = tuple([start])

        options = list(filter(self.key_starts_with(start), self.keys))
        if options:
          key = random.choice(options)
        else:
          return []
      else:
        key = random.choice(self.keys)
    else:
      key = self.make_key(seed)

    text = list(key)
    should_stop = False

    while (len(text) < length) and not should_stop:
      if (key in self.data):
        next_word = random.choice(self.data[key])
        text.append(next_word)
        key = key[1:] + tuple([next_word.lower()])

        if stop and callable(stop) and stop(next_word):
          should_stop = True
      else:
        return list(filter(lambda word: word != START_TOKEN and word != END_TOKEN, text))

    return list(filter(lambda word: word != START_TOKEN and word != END_TOKEN, text))

  def generate_syllable_length (self, length = 50, seed = None, start = None, stop = None, count_syllables = None):
    if seed is None:
      if start:
        if type(start) != tuple:
          start = tuple([start])

        options = list(filter(self.key_starts_with(start), self.keys))
        if options:
          key = random.choice(options)
        else:
          return []
      else:
        key = random.choice(self.keys)
    else:
      key = self.make_key(seed)

    text = list(key)
    syllable_count = sum([count_syllables(word) for word in key if key != START_TOKEN and key != END_TOKEN])
    should_stop = False

    while (syllable_count < length) and not should_stop:
      if (key in self.data):
        options = copy.copy(self.data[key])
        random.shuffle(options)
        next_word = options.pop(0)

        count_next_word = count_syllables(next_word)
        if not count_next_word:
          count_next_word = 3

        while len(options) > 0 and (syllable_count + count_next_word) > length:
          next_word = options.pop(0)
          count_next_word = count_syllables(next_word)
          if not count_next_word:
            count_next_word = 3

        text.append(next_word)
        key = key[1:] + tuple([next_word.lower()])

        syllable_count += count_next_word

        if stop and callable(stop) and stop(next_word):
          should_stop = True
      else:
        return list(filter(lambda word: word != START_TOKEN and word != END_TOKEN, text))

    return list(filter(lambda word: word != START_TOKEN and word != END_TOKEN, text))

