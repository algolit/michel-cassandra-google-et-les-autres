import json
import sys
import os.path

path = sys.argv[1]

if os.path.exists(path):
  with open(path, 'r') as h:
    data = json.load(h)
    sum_all = 0

    for l in data:
      print('***********')
      print(l)
      for y in sorted(data[l].keys()):
        ids = []
        year_count = len(data[l][y])
        sum_all += year_count
        print('{}: {}'.format(y, year_count))

        for article in data[l][y]:
          ids.append(article['id'])

        unique_ids = set(ids)

        if len(ids) != len(unique_ids):
          print('Has doubles.', len(ids) - len(unique_ids))

      print('---')
      print(sum_all)

else:
  print('Can not find path {}'.format(path))