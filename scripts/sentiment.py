# from pattern.fr import parsetree
from settings import ETRACES_DATA_FOLDER
from pattern.en import sentiment
# from pattern.vector import LEMMA, Document, Model, TFIDF
from parsing_etraces import parseAll, getAllArticles
from collections import Counter
from controlcodes import ERASE_LINE, RESET_CURSOR
import sys

if __name__ == '__main__':
  data = parseAll(ETRACES_DATA_FOLDER)
  articles = getAllArticles(data, 'en')
  data = []
  keyword_counter = Counter()

  for k, article in enumerate(articles):
    if k % 10 == 0:
      sys.stdout.write(RESET_CURSOR + ERASE_LINE)
      sys.stdout.write('{} / {}'.format(k, len(articles)))
      sys.stdout.flush()
    raw_text = article['title'] + '\n\n' + article['text']

    polarity, subjectivity = sentiment(raw_text)
    data.append((polarity, subjectivity, raw_text))
    # Load as a document, remove stopwords, lemmatize
    # document = Document(raw_text, name=article['id'], stopwords=False)
    # documents.append(document)

  sys.stdout.write(RESET_CURSOR + ERASE_LINE)
  sys.stdout.flush()
  data = sorted(data, key=lambda r: r[0])

  print(data[0:5])