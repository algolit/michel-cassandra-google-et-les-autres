import models
from utils import load_data, get_last_word
# from sqlalchemy import create_engine
from db import db_session, init_db
from datetime import date
from controlcodes import ERASE_LINE, RESET_CURSOR
import sys

from pattern.fr import parsetree as parsetree_fr
from rhymes_fr import rhyming_part as rhyming_part_fr, count_syllables as count_syllables_fr

from pattern.en import parsetree as parsetree_en
from rhymes_en import rhyming_part as rhyming_part_en, count_syllables as count_syllables_en

init_db()

def count_sentence_syllables (language, sentence):
  count = 0

  for word in sentence.words:
    if language == 'fr':
      count += count_syllables_fr(word.string)
    else:
      count += count_syllables_en(word.string)

  return count

def get_treeparser (language):
  if language == 'fr':
    return parsetree_fr
  else:
    return parsetree_en

def rhyming_part (language, word):
  if language == 'fr':
    return rhyming_part_fr(word)
  else:
    return rhyming_part_en(word)

for language in ['fr', 'en']:

  # engine = create_engine("sqlite+pysqlite:///{}.db".format(language), echo=False, future=True)

  data = load_data(language)

  language_model = models.Language(language=language)

  db_session.add(language_model)
  db_session.flush()

  # Dictionaries, caching created objects
  # to not have to hit the database all the time
  keyword_models = {}
  keyword_category_models = {}
  source_models = {}
  rhyme_models = {}

  # Return a keyword, if it doesn't
  # exist it's created
  def get_keyword(keyword, category):
    key = (keyword, category)

    if key not in keyword_models:
      keyword_models[key] = models.Keyword(
        keyword=keyword,
        keyword_lower=keyword.lower(),
        category=get_keyword_category(category),
        language=language_model
      )

      db_session.add(keyword_models[key])
      db_session.flush()

    return keyword_models[key]

  # Return a keyword category
  # will be created if it doesn't exist
  def get_keyword_category (category):
    key = (category,)

    if key not in keyword_category_models:
      keyword_category_models[key] = models.KeywordCategory(
        category=category,
        language=language_model
      )

      db_session.add(keyword_category_models[key])
      db_session.flush()

    return keyword_category_models[key]

  def get_source (source, url):
    key = (source, url)

    if key not in source_models:
      source_models[key] = models.Source(
        name=source,
        url=url,
        language=language_model
      )

      db_session.add(source_models[key])
      db_session.flush()
    
    return source_models[key]

  def get_rhyme (rhyme_part):
    key = (rhyme_part,)

    if key not in rhyme_models:
      rhyme_models[key] = models.Rhyme(
        rhyme=rhyme_part,
        language=language_model
      )

      db_session.add(rhyme_models[key])
      db_session.flush()

    return rhyme_models[key]

  for year in sorted(data[language].keys()):
    print(year)
    articles = data[language][year]
      
    for k, article in enumerate(articles):
      sys.stdout.write(RESET_CURSOR + ERASE_LINE)
      sys.stdout.write('{} / {} {}'.format(k+1, len(articles), article['title'][:80]))
      sys.stdout.flush()
      
      article_model = models.Article(
        title = article['title'],
        text = article['text'],
        url = article['source'][1],
        year = year,
        date = date(year=article['date'][2], month=article['date'][1], day=article['date'][0]),
        language=language_model
      )

      article_model.source = get_source(article['source'][0], '')

      db_session.add(article_model)
      db_session.flush()

      for (category, keywords) in article['keywords'].items():
        for keyword in keywords:
          article_model.keywords.append(get_keyword(keyword, category))

      parsed_text = get_treeparser(language)(article['text'], tokenize=True, tags=False, chunks=False, relations=False, lemmata=False)
      sentences = []

      # Parse sentences, make database entries
      for sentence in parsed_text.sentences:
        last_word = get_last_word(sentence).lower()
        syllable_count = count_sentence_syllables(language, sentence)
        sentences.append(models.Sentence(
          text = sentence.string,
          syllable_count = syllable_count,
          word_count = len(sentence.words),
          last_word = last_word,
          article = article_model,
          last_word_rhyme = get_rhyme(rhyming_part(language, last_word)),
          language=language_model
        ))

        for (category, keywords) in article['keywords'].items():
          for keyword in keywords:
            sentences[-1].keywords.append(get_keyword(keyword, category))

        db_session.add(sentences[-1])

      db_session.flush()

      sys.stdout.write(RESET_CURSOR + ERASE_LINE)

  db_session.commit()