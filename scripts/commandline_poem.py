from sqlalchemy import create_engine, select, func, bindparam
from db import db_session
from poem import make_poem
from time import sleep
from utils import make_abs_path
import random
import models
from settings import MAX_SENTENCE_LENGTH, MIN_SENTENCES

# lang = models.Language.filter_by(language=language).scalar().id
# wehere(models.Keyword.lanuage_id = lang.id)

languages = db_session.execute(select(models.Language)).scalars().all()

# Selects eligible keywords: keywords linked to sentence
# shorter than the maximum length and having a rhyme
eligible_keyword_query = select(models.Keyword)\
  .join(models.Keyword, models.Sentence.keywords)\
  .join(models.Rhyme)\
  .group_by(models.Keyword.id)\
  .where(
    models.Keyword.language_id == bindparam('language_id'),
    models.Sentence.word_count < MAX_SENTENCE_LENGTH,
    models.Rhyme.rhyme != ''
  )\
  .having(func.count(models.Sentence.id) >= MIN_SENTENCES)

eligible_keywords = {
  language.language: list(db_session.execute(
    eligible_keyword_query,
    { 'language_id': language.id}
  ).scalars()) for language in languages
}

print()
print()
while(True):
  language = random.choice(languages)

  keyword = random.choice(eligible_keywords[language.language])
  poem = make_poem(db_session, keyword)
  if poem:
    print()
    if language.language == 'en':
      print('On {}'.format(keyword.keyword))
    else:
      print('Sur {}'.format(keyword.keyword))
    print()
    for s in poem:
      print('{}'.format(s.text))
    print()
    print()
    sleep(1)
  else:
    print('Could not generate poem on {}'.format(keyword.keyword))