from parsing_etraces import parseAll
from collections import Counter
import datetime
from utils import normalize_source_url

print('Parsing data...')

parsed = parseAll('../data')

keywords_per_year = {}
seen_keywords = []


for lang, years in parsed.items():
  for year, articles in years.items():    
    if year not in keywords_per_year:
      keywords_per_year[year] = []

    for article in articles:
      keywords = [k.lower() for k in article['keywords']]

      for keyword in keywords:
        if keyword not in seen_keywords:
          seen_keywords.append(keyword)
          keywords_per_year[year].append(keyword)

output = []

for r in range(max([len(keywords) for keywords in keywords_per_year.values()])):
  output.append('')
  for year in keywords_per_year.keys():
    if r < len(keywords_per_year[year]):
      output[-1] += keywords_per_year[year][r]
    output[-1] += '\t'

print('\n'.join(output))