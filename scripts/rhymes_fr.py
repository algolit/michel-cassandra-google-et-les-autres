import csv
import math
import os.path

# { word: int:syllable_count, ... }
word_syllable_index = {}
# { word: str:rhyme_end }
word_rhyme_index = {}
# { str:rhyme_end: [str:word, ... ], ... }
rhyme_index = {}


def count_syllables (word):
  word = word.lower()

  if word in word_syllable_index:
    return word_syllable_index[word]
  else:
    return math.floor(len(word) / 2.5)

def rhyming_part (word):
  if word in word_rhyme_index:
    return word_rhyme_index[word]
  else:
    return ''

def rhymes (word):
  word = word.lower()

  if word in word_rhyme_index:
    # Lookup rhyming words in index
    rhyming_words = rhyme_index[word_rhyme_index[word]]

    # Return a list without the query word
    return list(filter(lambda w: w != word, rhyming_words))
  else:
    return []


def extract_rhyme (syllables):
  ## To do more intelligent
  #consonants = ['b', 'c', 'd', 'f', 'g', 'G', 'h', 'j', 'k', 'l', 'm', 'n', 'N', 'p', 'r', 'R', 's', 'S', 't', 'v', 'w', 'x', 'z', 'Z']
  vowels = ['a', 'e', 'i', 'o', 'u', 'y', 'A', 'E', 'I', 'O', 'U', 'Y', '1', '2', '5', '8', '9', '0', '°', '@', '§']
  
  chars = list(syllables.split('-')[-1])
  # print(chars)
  rhyme = []

  while len(chars) > 0:
    rhyme.append(chars.pop())
    if rhyme[-1] in vowels:
      rhyme.reverse()
      # print(''.join(rhyme))
      return ''.join(rhyme)
    # elif rhyme[-1] not in consonants:
    #   print(syllables)
    #   print(vowels, rhyme, rhyme[-1] in vowels)
    #   print('UNKNOWN CHAR ', rhyme[-1])
    #   exit()
    # # chars = last.unshift()

  return ''.join(rhyme)


with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data', 'Lexique383.csv'), 'r') as source:
  print('Loading lexicon')

  reader = csv.reader(source)

  next(reader)

  for row in reader:
    word = row[0]
    syllable_count = int(row[23])
    syllables = row[22]
    rhyme = extract_rhyme(syllables)
    
    word_syllable_index[word] = syllable_count
    word_rhyme_index[word] = rhyme

    if rhyme not in rhyme_index:
      rhyme_index[rhyme] = []

    rhyme_index[rhyme].append(word)