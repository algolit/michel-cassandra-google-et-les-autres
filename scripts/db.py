from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from utils import make_abs_path

dbpath = make_abs_path('data', 'etraces.db')
engine = create_engine("sqlite+pysqlite:///{}".format(dbpath), echo=False, future=True)
db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))
Base = declarative_base()
Base.query = db_session.query_property()


def init_db():
  import models

  Base.metadata.drop_all(bind=engine)
  Base.metadata.create_all(bind=engine)