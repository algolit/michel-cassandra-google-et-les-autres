import json
from markov import Markov, START_TOKEN, END_TOKEN, count_syllables
import random
import re
import pronouncing
import os.path
from rhymes_en import count_syllables

keyword = 'Google'
language = 'en'


data = json.load(open(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data', '{}.json'.format(language))))

if language == 'fr':
  from pattern.fr import parsetree
elif language == 'en':
  from pattern.en import parsetree
else:
  from pattern.nl import parsetree

corpus = []
models = []

"""
{
  'fr': {
    '2007': [
      {
        'id': 'id',
        'title': 'title',
        'date': 'date',
        'keywords': {
          'company': [],
          'web': [],
          'tools': [],
          'subject': [],
          'citizen_protection': [],
        },
        'zone': [],
        'type': []
        'text': 'text',
        'source': (title, url)
      },
    ]
  },
}
"""

def has_keyword_filter (article):
  return keyword in article['keywords']['company']

# Loop through the years and construct markov chains
for year in ['2020']: #sorted(data[language].keys()):
  print(year)
  if year in data[language]:
    corpus.append(list(filter(has_keyword_filter, data[language][year])))

    if len(corpus[-1]) > 0:
      markov_generator = Markov(window=2)
      reverse_markov_generator = Markov(window=2)

      # Set article list in random order, as not all of them ar processed
      random.shuffle(corpus[-1])

      for article in corpus[-1][:500]:
        parsed_text = parsetree(article['text'], tokenize=True, tags=False, chunks=False, relations=False, lemmata=False)
        # tokens = [[word.string for word in sentence.words] for sentence in parsed_text.sentences]
        tokens = []

        for sentence in parsed_text.sentences:
          # Loop through sentences and add them to the model
          tokens.extend([START_TOKEN] + [word.string for word in sentence.words] + [END_TOKEN])

        markov_generator.read(tokens)
        tokens.reverse()
        reverse_markov_generator.read(tokens)

      models.append((markov_generator, reverse_markov_generator))
    else:
      models.append((None, None))
  else:
    models.append((None, None))

# Make a string out of a list of words
def make_text (words):
  text = ''
  first_word = True
  captilize = True

  for word in words:
    if re.match(r'\W', word) is not None:
      text += word

      if re.match(r'[!?\.]+$', word) is not None:
        captilize = True

      if re.match(r'\n+$', word) is not None:
        first_word = True

    else:
      if not first_word:
        text += ' '

      text += word.capitalize() if captilize else word
      captilize = False
      first_word = False

  return text

for model, reverse_model in models:
  if model:
    for _ in range(10):
      rhyming_words = []
      rhyming_line = []
      
      # Generate a line
      line = model.generate(15, start=START_TOKEN, stop=lambda w: w == END_TOKEN)

      print(line)

      # Loop throught the line
      while line and not rhyming_words:
        if re.match('\W', line[-1]):
          # If last word has interpunction drop it
          line.pop(-1)
        else:
          # Find rhyming words for the last word
          rhyming_words = pronouncing.rhymes(line[-1])

          if rhyming_words:
            syllable_count = 0

            for word in line:
              syllable_count += count_syllables(word)
            
            # If rhyming words are found, set at random order
            random.shuffle(rhyming_words)

            # Try to generate a sentence
            # if it fails pick next rhyming word
            # and try again
            while rhyming_words and not rhyming_line:
              rhyme_word = rhyming_words.pop(0)
              rhyming_line = reverse_model.generate_syllable_length(syllable_count, start=('.', rhyme_word), count_syllables=count_syllables)
              rhyming_line.reverse()

            # Could not create a rhyming line for the current
            # last word in the original sentence.
            # Remove that last word and try again
            if not rhyming_line:
              line.pop(-1)
          else:
            # No rhyming options for this word. Remove and try
            # with the next
            line.pop(-1)

      print()
      print(make_text(line))

      if rhyming_line:
        print(make_text(rhyming_line))
      else:
        print('***')

  else:
    print('***')

  print('')