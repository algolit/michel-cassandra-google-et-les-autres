"""
  This example script will ask the user for a sentence and then look up a
  sentence ending in a similar rhyme and has the same syllable count.
"""

from sentence_utils import find_language, find_sentence
from db import db_session

# Function to parse text
from pattern.fr import parsetree

# Functions to extract rhyming part and count the syllables
from rhymes_fr import rhyming_part, count_syllables

# Utility function to find the last word in the sentence that
# only has alphabetical characters (a → z)
from utils import get_last_word

def count_sentence_syllables (sentence):
  count = 0

  for word in sentence.words:
    count += count_syllables(word.string)

  return count

language = find_language(db_session, 'fr')

# Ask for a sentence
print('Can I ask you for a sentence?')
sentence = input('>')

parsed = parsetree(sentence, tokenize=True, tags=False, chunks=False, relations=False, lemmata=False)

sentence = parsed.sentences[0]
print('The sentence split into words')
print(sentence.words)
print()

# Count the syllables in the sentence.
syllable_count = count_sentence_syllables(sentence)
print('Counted', syllable_count, 'syllables')
print()

# Extract the last word
last_word = get_last_word(sentence)
print('Found', last_word, 'as the last word')
print()

# Look up the rhyme
rhyme = rhyming_part(last_word)
print('Found ', "'{}'".format(rhyme), 'as rhyme for the last word.')
print()

if rhyme == '':
  print('Could not find the rhyme. Cannot find a rhyming sentence :-(')
else:
  # Extract the rhyming part from the sentence
  rhyming_sentence = find_sentence(db_session, rhyme=rhyme, syllable_count=syllable_count, language=language)
  print(rhyming_sentence.text)



  """
    By querying the database directly it's possible to filter more precisely.
    By example it's possible to  prevent the rhyming sentence from ending in the same
    word as the sentence entered by the user.
  """

  import models
  from sqlalchemy import select, func

  other_rhyming_sentence = db_session.execute(select(models.Sentence).join(models.Rhyme).where(
    models.Sentence.last_word != last_word,
    models.Rhyme.rhyme == rhyme,
    models.Sentence.syllable_count == syllable_count,
    models.Sentence.language_id == language.id
  ).order_by(func.random()).limit(1)).scalar()

  if other_rhyming_sentence:
    print(other_rhyming_sentence.text)
  else:
    print('Could not find a sentence (with a rhyming word other than \'{}\').'.format(last_word))
