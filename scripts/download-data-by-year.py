import requests
from time import sleep
from parsing_etraces import parseString
import json
from sys import argv
from random import randrange

# word_ids = [1205]

languages = [ 'de', 'en', 'es', 'fr', 'it', 'nl', 'pt_br' ]

languages = [ 'en', 'fr', 'nl' ]

year = list(range(2007, 2022))

def make_api_url(language, year):
  return "https://etraces.constantvzw.org/informations/spip.php?page=export_agolight&lang={0}&quand={1}-01-01".format(language, year)


data = {}

l = language = argv[1]

print(language)

# for l in languages:
for y in year:
  r = requests.get(make_api_url(l, y))

  print(l, '--', y)
  if r.status_code == 200:
    if r.text:
      articles = parseString(r.text, l, y)
      
      if len(articles) > 0:
        print('Found {} article(s)'.format(len(articles)))

        if l not in data:
          data[l] = {}
  
        if y not in data[l]:
          data[l][y] = []

        data[l][y].extend(articles)
        json.dump(data, open('data-new-{}.json'.format(language), 'w'), ensure_ascii=False)

    sleep(float(randrange(50,250)/1000))
