import models
from sqlalchemy import select, func
from db import db_session

"""
  And example of how to query the database directly.
"""

stmt = select(models.Sentence).join(models.Rhyme).where(
    models.Sentence.last_word == 'request'
  ).order_by(func.random()).limit(1)

sentence = db_session.execute(stmt).scalar()

print(sentence.text)

stmt = select(models.Sentence).join(models.Rhyme).where(
    models.Sentence.last_word != sentence.last_word,
    models.Sentence.last_word_rhyme_id == sentence.last_word_rhyme_id
  ).order_by(func.random()).limit(1)

rhyming_sentence = db_session.execute(stmt).scalar()

print(rhyming_sentence.text)