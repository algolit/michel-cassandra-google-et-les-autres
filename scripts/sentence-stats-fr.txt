surveillance 8409
BigData 6366
algorithme 6174
Google 5264
Facebook 4867
santé 3790
smartphone 3678
vidéo-surveillance 3465
technologisme 3290
CCTV 2978
GigEconomy 2930
travail 2804
Amazon 2738
géolocalisation 2544
biométrie 2539
CNIL 2446
Apple 2296
COVID-19 2293
domination 2237
publicité 2180
profiling 2152
DataBrokers 2088
écoutes 1893
Microsoft 1847
facial 1746
données 1660
discrimination 1652
SocialNetwork 1608
copyright 1563
bénéfices 1555
législation 1512
biais 1408
manipulation 1407
marketing 1352
lutte 1301
reconnaissance 1281
Twitter 1263
hacking 1258
RGPD 1251
comportement 1243
bug 1227
LaQuadratureduNet 1221
censure 1210
GAFAM 1192
activisme 1144
écologie 1096
consentement 1033
modération 1020
FAI 995
GoogleSearch 934
contactTracing 917
anti-terrorisme 913
biopolitique 911
conditions 905
racisme 893
YouTube 865
procès 861
NSA 816
police 809
violence 770
sexisme 746
harcèlement 737
consommation 728
drone 728
Orange 690
Android 673
domotique 664
prédiction 646
enseignement 644
StopCovid 638
P2P 614
élections 611
payement 603
UberEATS 598
GPS 597
Bluetooth 594
Uber 582
HADOPI 569
visioconférence 555
éthique 549
5G 540
iPhone 521
syndicat 503
Thalès 497
journalisme 470
cartographie 467
cryptage 460
voyageurs 456
fiscalité 455
Gmail 450
spyware 447
SmartCity 446
Instagram 440
viol 440
aérien 434
neutralité 427
microtargeting 409
CambridgeAnalytica/Emerdata 401
InternetOfThings 400
LGBT 400
anonymat 398
CGT 394
lobbying 385
voiture 379
notation 375
addiction 368
AMESys 366
télétravail 363
militaire 359
Alibaba 359
IBM 352
délation 351
CloudComputing 347
exportation 343
Deliveroo 342
câble 340
CIA 339
Islam 335
extrême-droite 333
compagnie 328
jeu 319
Huawei 315
cookies 314
FBI 312
migration 312
criminalité 311
WhatsApp 310
Wikileaks 308
urbanisme 308
Altaba/Yahoo ! 306
GoogleMaps 306
ACTA 306
banque 303
IGPN 303
Netflix 302
APD-Belgique 300
TraceTogether 294
EFF 291
PRISM 286
enfants 283
LOPPSI 283
WiFi 278
robotique 277
FoodTech 275
passeport 274
femmes 270
SFR 267
Palantir 265
Technopolice 265
pornographie 262
Maven 256
brevet 255
Bull 255
historique 254
prison 254
bot 253
selfie 243
scraping 241
FTC 234
Bouygues 229
Samsung 228
WeChat 223
AppleStore 221
oubli 220
bracelet 220
DeepPacketInspection-DPI 218
Chrome 214
Alexa 214
QRcode 213
Alphabet 210
Nokia Siemens 207
température 207
émotions 205
Anduril 203
Tencent 201
iOS 199
GoogleStreetView 197
pauvreté 197
Skype 195
pédophilie 195
Free 194
empreintes 194
CJUE 192
voix 191
EAGLE 186
Airbnb 185
capteur 185
Cisco 183
Alicem 183
ResearchInMotion-RIM 182
LinkedIn 181
GoogleAdWords 178
Livre électronique 177
fraude 177
SocialNetworkGaming 174
SNCF 174
SocialCreditSystem 173
Bing 171
Qosmos 170
AmnestyInternational 170
Ubisoft 169
iTunes 165
religion 165
Two-I 165
LDH-France 163
FranceTelecom 161
lunettes 161
iPad 159
carte 155
tablette 155
BlackBerry 152
Criteo 152
PrivacyInternational 150
famille 150
RFID 148
STIC 148
eBay 147
ANSSI 147
justice 146
art 144
MinorityReport 144
BlackLivesMatter 141
TikTok 140
conducteur·trice·s 139
AmazonsPrime 137
MySpace 136
génétique 135
McDonald's 135
Google+ 135
InterHop 135
LoisurleRenseignement-France 133
CloudAct 133
Messenger 132
DGSE 130
interopérabilité 129
finance 129
HealthDataHub 128
recrutement 126
ACLU 124
LoiAvia 124
voisinage 124
PEPP-PT 123
TAJ 122
RATP 120
Baidu 120
minerais 118
Zoom 118
Safran 117
féminisme 116
GCHQ 116
Sony-Ericsson 114
LaPoste 112
BitTorrent 112
immatriculation 112
Smals 112
ARCEP 111
MeToo 110
Morpho 109
Ring 109
Safari 108
SOPA 108
obsolescence 108
Linky 107
RSF 105
DoubleClick 104
Flickr 103
Verizon 103
MegaUpload 103
Siri 103
Coronalert 103
DailyMotion 102
Spotify 102
Alipay 102
Kindle 101
Intel 101
Paypal 100
Cellebrite 100
LDH-Belgique 99
Snapchat 99
SUDSolidaires 99
G29 97
frontières 97
AT&amp;T 96
Wikipedia 95
Ufed 95
Dassault 95
corruption 95
Home 94
Firefox 93
Trident Media Guard (TMG) 93
PNIJ 93
ICE 93
IMSI-catchers 92
Echo 92
ATTAC 91
PatriotAct 90
AmazonWebServices-AWS 90
Libra 89
Idemia 89
Foursquare 88
Mozilla 87
vote 86
Ikea 86
Qwant 86
23andMe 85
FrenchDataNetwork-FDN 85
Framasoft 85
Vodafone 84
GooglePlay 83
Xiaomi 83
GoogleAdSense 82
LVMH 82
FacebookTimeline 82
phishing 81
Smart Grid 81
DGSI 81
WholeFoods 81
CFDT 81
FacebookLike 80
Waze 80
Pegasus 80
Doctolib 80
ByteDance 80
cryptomonnaie 78
Tinder 78
Ubérisation 78
NSO 78
Clearview 78
XKeyscore 77
NRA 76
Atos 76
BHATX 76
iris 75
AOL 74
jeunesse 74
Hikvision 74
AmazonMechanicalTurk 73
TES 73
JUDEX 72
AggregateIQ 72
DuckDuckGo 71
Tesla 70
backdoor 69
DeepMind 69
DP-3T 69
Société des Auteurs, Compositeurs et Editeurs de Musique (SACEM) 68
Area SpA 68
Rekognition 68
licenciement 68
Azure 67
bitcoin 67
ScotlandYard 67
LoiSécuritéGlobale 67
AlgorithmWatch 66
Meetic 65
Airbus 65
OVH 65
sport 65
Dragonfly 65
Engie 65
main 64
montre 64
EPIC 63
HackingTeam 63
DeutscheTelekom 62
TheGreatFirewallofChina 62
RIAA 62
Monsanto 62
Mastodon 62
Oracle 61
INDECT 61
documentaire 61
Leboncoin.fr 61
HumanRightsWatch-HRW 61
Nest 61
masque 61
Match 60
Motorola 60
Visa 60
Yelp 60
BND 60
panopticon 59
Disney 59
ConseilConstitutionnel-FR 59
DP3T 59
FacebookBeacon 58
FNAEG 58
LPM 58
NewsCorp 57
iWatch 57
Nike 57
arme 57
Face.com 56
Elektron 56
MasterCard 55
CapGemini 55
vieillesse 55
STIB-MIVB 54
ContentID 54
Mossad 54
PROTECTIP 54
ENEDIS 54
clavier 54
Stuart 54
AndroïdMarket 53
vidéo-verbalisation 53
mouvement 53
Coca-Cola 53
FluxVision 53
Datakalab 53
OkCupid 52
Picasa 52
wearable 52
DMCA 51
BlueCoat Systems Inc. 51
Briefcam 51
MSN 50
iPod 50
CNCDH 50
Windows 50
Fitbit 50
VPN 49
DJI 49
In-Q-Tel 48
GoogleDocs 48
MPAA 48
SSL/TLS 48
TripAdvisor 48
Windows Phone 48
Axon 48
sonnette 48
Carrefour 47
CDiscount 47
Adobe 46
Linux 46
booking.com 46
EDVIGE 45
VirtualReality 45
Alcatel-Lucent 45
Gemalto 45
streaming 45
QuantifiedSelf 45
puce 45
beauté 45
Humanyze 45
Brave 45
FreeSoftware 44
FCC 44
Utimaco 44
Lustre 44
AdBlock 44
DoD 44
Walmart 44
Sidep 44
ContactCovid 44
Smartcheckr 44
FNAC 43
Internet Explorer 43
Galaxy 43
Fédération internationale des droits de l'homme (FIDH) 43
Robocopyright 43
WeChatPay 43
Crypto 43
Xbox 42
Grindr 42
Rapidshare 42
Cortana 42
BigPharma 42
CLAP 42
Frontex 41
CitizenLab 41
AntFinancial 41
Médiapost 40
DRM 40
KPN 40
Pokémon 40
Lyft 40
forme 40
Reporty 40
Onhys 40
agriculture 40
BASE 39
Ford 39
PrivacyShield 39
HireVue 39
Eiffage 39
Endor 39
Nexus 38
Assistant 38
AntGroup 38
PNR 37
usager 37
Big Brother Awards 37
TV 37
MicrosoftHotmail 37
Comcast 37
TF1 37
ZTE 37
The Pirate Bay 37
ChaosComputerClub-CCC 37
Salesforce 37
Renault 37
FranceConnect 37
JustEat 37
Mobib 36
serveur 36
Pagejaunes.fr 36
Stuxnet 36
Diaspora 36
eMule-Paradise 36
GoogleShopping 36
antisémitisme 36
Accenture 36
Equifax 36
CCPA 36
FightfortheFuture 36
Société Belge des Auteurs, Compositeurs et Editeurs (SABAM) 35
GoogleLatitude 35
HTC 35
HP 35
TOR 35
marche 35
GoogleCloud 35
ransomware 35
Acxiom 35
DoJ 35
Swift 34
MetropolitanPolice 34
Zynga 34
GoogleBlogger 34
ePrivacy 34
Interpol 33
Belgacom 33
spam 33
DNS 33
Symantec 33
Loi pour la Confiance dans l'Économie Numérique (LCEN) 33
Oculus 33
Alstom 33
milice 33
thisisyourdigitallife 33
RealTimeBidding-RTB 33
Torrent 32
Hachette 32
Bayer 32
influenceur 32
Commissaire Ã la protection de la vie privée (Canada) 31
Nestlé 31
Greenpeace 31
Deezer 31
Europol 31
CSA 31
Weibo 31
FISA 31
Edge 31
Verily 31
BlackRock 31
SDK 31
Navigo 30
GoogleEarth 30
Groupon 30
Oberthur 30
Cognitec 30
Tadata 30
Rakuten 29
ICANN 29
Securitas 29
Atari 29
BigBrotherWatch 29
Opera 29
Google X 29
Niantic 29
Nightingale 29
EADS 28
E-PARASITES 28
WifiCatcher 28
SidewalkLabs 28
transhumanisme 28
Softbank 28
SpaceX 28
EDF 27
Net Users' Rights Protection Association (NURPA) 27
DoNotTrack-DNT 27
SIM 27
Path 27
Upstream 27
sommeil 27
son 27
Starlink 27
SafeHarbor 26
Domino'sPizza 26
PageRank 26
Yandex 26
Dell 26
Kelkoo 26
Nintendo 26
FAED 26
Organisation Mondiale de la Propriété Intellectuelle (OMPI) 26
Weborama 26
Tesco 26
Watson 26
Telegram 26
BNP-Paribas 26
Frichti 26
Taobao 25
Nametag 25
Qualcomm 25
Monoprix 25
Vinci 25
Pasp 25
Gipasp 25
Sandbox 25
scanner 24
CAPTCHA 24
Orkut 24
Boeing 24
PriceMinister 24
Telenet 24
GoogleNews 24
Pfizer 24
Crescendo Industries 24
Gowalla 24
LG 24
RemoteControlSystem 24
DeepFace 24
Experian 24
jouet 24
ProtonMail 24
Reliance 24
Jio 24
Twitch 24
EuropeanDataJournalismNetwork 24
COVIDSafe 24
ElectronicArts 23
GoogleAnalytics 23
Starbucks 23
FacebookPlaces 23
Kinect 23
Swisscom 23
Wikimedia 23
Arabic Network for Human Rights Information (ANHRI) 23
Parrot 23
CNCTR 23
HomePod 23
URL 23
DoorDash 23
PirateBay 22
Roskomnadzor 22
GoogleCalendar 22
Nexa 22
BostonDynamics 22
FaceID 22
ALPA 21
Anonymous 21
L'Oréal 21
Gamma 21
PalTalk 21
PôleEmploi 21
Total 20
i2e 20
SS8 20
Alise 20
tatouage 20
Mystic 20
ApplePay 20
Darty 20
Proximus 20
AshleyMadison 20
Otsuka 20
Sanofi 20
Fireworld 20
Quayside 20
Novartis 20
RakningC-19 20
Lidl 19
Service général du renseignement et de la sécurité (SGRS - Belgique) 19
Société des Producteurs de Phonogrammes en France (SPPF) 19
miroir interactif 19
DPC-Ireland 19
Packet Forensics 19
FinFisher 19
YouPorn 19
Tempora 19
UnitedHealthGroup 19
SociétéGénérale 19
Sésame 19
Nextdoor 19
EASP 19
Simulmatics 19
licence globale 18
Virgin 18
Contrôleur européen de la protection des données (CEPD) 18
ChinaMobile 18
GoogleTV 18
SACD 18
Sprint 18
Vimeo 18
Telenor 18
SPEI 18
Shoghi Communications 18
Trace Span 18
Sleuth-Hound Software 18
Navigator 18
Phantom 18
Twitter Trends 18
Adidas 18
Predpol 18
FiveEyes 18
ELeclerc 18
Athena 18
sextorsion 18
DidiChuxing 18
GE 18
Glovo 18
mSpy 18
Gendnotes 18
domicile 18
ShinBet 18
QAnon 18
Tamoco 18
BanqueNationaleGénérale-BNG 18
GoogleHealth 17
Kaspersky 17
British Phonographic Industry (BPI) 17
BankofAmerica 17
DropBox 17
Cybersecurity Information Sharing Act (CISA) 17
Reddit 17
Garmin 17
Withings 17
MIT 17
Foodora 17
MyHeritage 17
KGB 17
Anyvision 17
Adecco 17
Cognizant 17
Eco-Compteur 17
SpyStealth 17
Macy’s 17
Spock 16
Telefonica 16
Cryptome 16
BritishTelecom-BT 16
RIPA 16
Blizzard 16
T-Mobile 16
Time Warner 16
IPRED 16
keylogger 16
Numericable 16
Flame 16
FinSpy 16
G4S 16
Optic Nerve 16
MUSCULAR 16
PSA 16
Toyota 16
greenwashing 16
TakeItEasy 16
Fairphone 16
EncroChat 16
Section230 16
PGP 15
Digital Economy Act 15
ChinaTelecom 15
ICO-UK 15
Commission d'accès aux documents administratifs (CADA) 15
Universal 15
FacebookShadowProfiles 15
Hotfile.com 15
télévision 15
Pepsi-Cola 15
GoogleImages 15
MI5 15
NEC 15
Centre de la sécurité des télécommunications Canada (CSTC) 15
SOMALGET 15
Unilever 15
GEDmatch 15
Star Service 15
Tok Tok Tok 15
Fiat 15
CoopCycle 15
CoursiersBordelais 15
PlayStation 14
Center for Democracy and Technology (CDT) 14
NHS 14
GoogleInstant 14
ActUp 14
OCDE 14
GoogleDrive 14
DigiNotar 14
Screenwise 14
NYPD 14
Verisign 14
Apache 14
SceneTap 14
CETA 14
Cour européenne des droits de l'Homme 14
Expedia 14
Milestone 14
Johnson&amp;Johnson 14
SCL 14
PornHub 14
Weenect 14
ExtinctionRebellion 14
AmisdelaTerre 14
tampon 14
AarogyaSetu 14
FLoC 14
VideoBB 13
EMI 13
KaZaA 13
Zagat 13
Sagem 13
Echelon 13
Foxconn 13
FaceTime 13
AngryBirds 13
Loi sur le renseignement (Suisse) 13
GCSB 13
Shazam 13
Telmex 13
Leti 13
Yahoo Messenger 13
AXA 13
Kairos 13
Volkswagen 13
Signal 13
PredVol 13
Paved 13
instacart 13
Wish 13
Parler 13
Axone 13
Postmates 13
ComputerPeopleforPeace 13
Spie 12
DHS 12
Vkontakte 12
locataires 12
avortement 12
Keeneo 12
Video Synopsis 12
Viewdle 12
GeneralMotors-GM 12
Pinterest 12
Reppify 12
Rosoboronexport 12
Syrian Telecommunication Establishment (STE) 12
WesternUnion 12
AppleiCloud 12
Ercom 12
RAMPART 12
NineEyes 12
FourteenEyes 12
Epsilon 12
Datalogix 12
AncestryDNA 12
JD.com 12
Peertube 12
JEDI 12
NOYB 12
TaskRabbit 12
OpenMarkets 12
SyRI 12
CBP 12
3M 12
uSocial.net 11
OpenRightsGroup 11
AdMob 11
World of Warcraft 11
Stratfor 11
Société Civile des Producteurs Phonographiques (SCPP) 11
Netfirms 11
Narus 11
BAE 11
Oceatech Equipement 11
veines 11
Orélia 11
Kaolab 11
Spikenet Technology 11
Sarnof 11
Klik 11
Guardian Project 11
Tumblr 11
Trovicor 11
CNCIS 11
Wassenaar 11
RichRelevance 11
Predictive Mix 11
FireChat 11
JCDecaux 11
MBDA 11
GitHub 11
Ralph Lauren 11
Mosaic 11
Taser 11
supermarché 11
AccessNow 11
Strava 11
Portal 11
Rolls Royce 11
Immuni 11
DataJust 11
XXII 11
AOL 10
Scarlet 10
Canal+ 10
TalkTalk 10
FacebookSponsoredStories 10
SNCB-NMBS 10
UFC-Que choisir 10
GoldmanSachs 10
Syndicat National de l'Édition Phonographique (SNEP) 10
CarrierIQ 10
IQ Insight Experience Manager 10
Fédération Anti-Piratage Belge (FAB) 10
Vitale 10
Napster 10
Redigi 10
Auchan 10
Netscape 10
Jeux olympiques 10
SyriaTel 10
WiretapAct 10
Semptian 10
iMessage 10
GoogleTranslate 10
PARAFE 10
Verdi 10
Lacoste 10
Skynet 10
Jawbone 10
Blablacar 10
trottinette 10
SNJ 10
Serenicity 10
Delair 10
FlyingEye 10
Deepomatic 10
Hypervision3D 10
FSF 9
CenterforDigitalDemocracy 9
Electronic Frontiers Australia 9
Viacom 9
Creatives Commons 9
Playdom 9
Playfish 9
British Petroleum (BP) 9
FoxNews 9
US Copyright Group 9
Digital Rights Ireland 9
BaiduMaps 9
Twitter Follow 9
Apple Corp (Beatles label) 9
Paris Habitat 9
Orangina 9
TrapWire 9
JPMorgan 9
Gandi 9
UniqueIdentificationAuthorityofIndia-UIDAI 9
Raytheon 9
Rapid Information Overlay Technology (RIOT) 9
OTAN 9
Lecrec 9
pouls 9
Heartbleed 9
Citigroup 9
Lego 9
AirFrance 9
CEDH 9
QQ 9
Aladdin 9
Nvidia 9
Pinduoduo 9
Refog 9
Takeaway.com 9
Meituan 9
UCGlobal 9
Teams 9
Rapleaf 8
humanrights21 8
Internet Watch Foundation (IWF) 8
blog 8
Warner Bros 8
Rentafriend.com 8
GoogleVoice 8
FacebookMessages 8
ChinaUnicom 8
VASTech 8
United States Patent and Trademark Office (USPTO) 8
BlackBerryMessenger-BBM 8
DADVSI 8
Bell 8
Voo 8
FSB 8
Xiti 8
Sky 8
O2 8
DataSift 8
Rumblefish 8
TTIP/TAFTA 8
Quick 8
Outlook 8
Viber 8
Ministère français de l'Economie et des Finances (Minefi) 8
Dropcam 8
Tails 8
Sherpa 8
TrueCrypt 8
Turbomeca 8
Zodiac Data Systems 8
TCS 8
Latecoere 8
OpenGarden 8
OTR (Off-the-Record Messaging) 8
Ancestry.com 8
BMW 8
Trygve 8
tracker 8
Microscan 8
Quidsi 8
SenseTime 8
Sensorvault 8
HalteàlObsolescenceProgrammée-HOP 8
Human Rights in China 8
ProductivityScore 8
NAACP 8
TopEspion 8
AddThis 8
Monster 7
Mozilla Foundation 7
PublicKnowledge 7
Project Titan 7
Pandora 7
Office Européen des Brevets (OEB) 7
Internet Sans Frontières 7
CreativeCommons 7
iBook 7
VisualRevenue 7
Ligatus 7
iPhoto 7
Sina 7
Ethio 7
Legal Intercept 7
La Redoute 7
Facewatch 7
COPPA 7
GoogleWallet 7
Nedap 7
Lenovo 7
Smart Sensor 7
IP Tracking 7
DARPA 7
DGCCRF 7
Babar 7
Operation Socialist 7
ExxonMobil 7
Sofrecom 7
GooglePhotos 7
Service de renseignement de la Confédération (SRC) - Suisse 7
GlaxoSmithKline 7
sciences 7
bossware 7
Mobileye 7
Neighbor 7
doctissimo.fr 7
Grubhub 7
Anticor 7
DéfenseurdesDroits 7
Giphy 7
e-yuan 7
Discord 7
Banjo 7
KKK 7
MicrosoftHealthVault 6
Google-watch 6
Wink 6
Parti Pirate 6
Elbit 6
Sun Microsystems 6
LimeWire 6
ACS:Law 6
Logistep 6
Daimler 6
International Trade Commission (ITC) 6
COICA 6
Quantcast 6
Viadeo 6
Oxfam 6
EuropeanDigitalRights-EDRi 6
Xunlei 6
Bescherming Rechten Entertainment Industrie Nederland (BREIN) 6
Nice 6
Verint 6
Sophos Ltd. 6
American Censorship 6
Smart Connected Communities 6
Uplay 6
USArmy 6
FDA 6
LCI 6
Atlas 6
vêtement 6
Agencia Española de Proteccion de Datos (AEDP) 6
odeur 6
MorphoArgus 6
Shodan 6
ClearChannel 6
MobinNet 6
ixMachine-LI 6
Tungstène 6
Happn 6
sextoy 6
FaceApp 6
Wannacry 6
RT 6
Sputnik 6
Waymo 6
StatusToday 6
Face 6
École42 6
Bolt 6
Kapten 6
Nuance 6
Sam 6
Liberties 6
GoogleVisionAI 6
uBlockOrigin 6
Conseild'État-FR 6
Zara 6
satellite 6
Hamagen 6
JioMart 6
ElliottManagementFund 6
Tyco Healthcare 5
Ask.com 5
wikipedia 5
GoogleVideos 5
BundesKriminalAmt (BKA) 5
Quattro Wireless 5
Tiscali 5
GoogleTalk 5
CCIA 5
The Daily 5
C-51 (Canada) 5
National Music Publisher's Association (NMPA) 5
Canon 5
KindSight 5
Twitpic 5
RespectMyNet 5
IsoHunt 5
Association des Services Internet Communautaires (ASIC) 5
Motion Picture Association (MPA - UK) 5
Système d'information Schengen (SIS) 5
UE Eurodac Europol Eurojust VIS 5
Saab 5
Prûm 5
Kobo 5
OpenStreetMap 5
Ixquick 5
Centre d'Etudes sur la Citoyenneté, l'Informatisation et les Libertés (CECIL) 5
Ticketmaster 5
Vupen Security 5
Sûreté International 5
Selex Elsag 5
Macif 5
Second Life 5
Aadhaar 5
Yahoo Mail 5
Whisper 5
Global Privacy Enforcement Network (GPEN) 5
Target 5
Wifeel 5
AdopteUnMec.com 5
FacebookWorkplace 5
Juniper 5
AppleHealthKit 5
BritishAirways 5
Netatmo 5
Mensura 5
Galileo 5
chômage 5
Aetna 5
HSBC 5
Teemo 5
Trooly 5
GenesisToys 5
23Mofang 5
Suneris 5
Slack 5
Napatech 5
UPS 5
IJOP 5
Nieuw-Vlaamse Alliantie (N-VA) 5
Leonardo 5
Wyze 5
geofencing 5
S2ucre 5
Square 5
Chevron 5
Moovit 5
Robinhood 5
Pretty Good Privacy 4
del.icio.us 4
Samuelson Law, Technology and Public Policy Clinic 4
Seppukoo 4
Nook 4
AutoritédelaConcurrence 4
Labpixies 4
Consumer Watchdog 4
Symbian 4
TiP2Pay 4
Sky Digital Technologies 4
uTorrent 4
BoozAllenHamilton 4
VSD 4
Bits of Freedom 4
Cablevision 4
Motion Picture Patent Company (MPPC) 4
Préposé Fédéral Ã la Protection des Données et Ã la Transparence (PFPDT - Suisse) 4
HERISSON 4
Fujitsu 4
InternationalFederationofthePhonographicIndustry-IFPI 4
WebM 4
Measurement Lab 4
Visualizing Yahoo ! 4
Kobo Reading Life 4
animaux 4
Frenchelon 4
Redpepper 4
United Nations Office against Drugs and Crime (UNODC) 4
Ramnit 4
The Find 4
Unique Device Identifier (UIDAI) 4
China Labor Watch 4
Ziggo 4
XS4LL 4
Marketly 4
Yfrog 4
Service de l'Informatique Cantonal (SDI) 4
Commission cantonale de Protection des Données (CDP - Suisse) 4
modem 4
MapMyFitness 4
Web MD Healh 4
Nviso 4
fingerprinting 4
Cryptocat 4
MTN 4
Wickr 4
Tobasco 4
Z-Staffing 4
Nike FuelBand 4
Reinnav 4
Mantatech 4
LVLF 4
Zephyros Invest 4
MI6 4
THE THREE SMURFS "&#8220; Dreamy, Nosey et Tracker 4
Iliad 4
Clear 4
InternetArchive 4
europe-v-facebook 4
Replika 4
AppleFaceID 4
Marriott 4
Pixar 4
CloudWalk 4
SoundCloud 4
Bird 4
Randstad 4
EHTERAZ 4
Smittestopp 4
Axis 4
Noldus 4
SafeEntry 4
Sightcorp 4
Revlon 4
TousAntiCovid 4
MinistryofPrivacy 4
Datenschutzbeauftragten (Allemagne) 3
kill switch 3
CNN 3
Yahoo Right Media 3
ULM 3
Valve 3
Netezza 3
PourEva 3
SK Marketing 3
Freedom of Information Act (FOIA) 3
Teachbook 3
Préposé Ã la protection des données (Suisse) 3
ECPA 3
Trombi 3
Rogers 3
antiloppsi2.net 3
Mégavideo 3
Credit Suisse 3
Renren 3
Dish Network 3
Magic Lantern 3
ICO 3
Sinde 3
Office Central de Lutte contre la Criminalité liée aux Technologies de l'Information et de la Communication (OCLCTIC) 3
Commission des libertés civiles (LIBE) 3
ITA-India 3
CALEA 3
Recording Industry Association of Japan (RIAJ) 3
GreenIT 3
Union internationale des télécommunications (UIT) 3
NorthropGrumman 3
Odebrecht 3
Tepro 3
Kobojo 3
NetApp Inc. 3
VMWare 3
Red Hat 3
RiotGames 3
Morgan Stanley 3
NetBio 3
Rapid DNA 3
RapidHIT 3
Near Field Communication (NFC) 3
Rovio 3
Quechua 3
Russian Mobile Operating System (RoMos) 3
Yoocasa 3
Center for Copyright Information (CCI) 3
Syrian Computer Society (SCS) 3
Leroy Merlin 3
Xerox 3
Knightscope 3
Knightscope K5 3
Barclays 3
Topsy Labs Inc. 3
chaussure 3
Pony 3
Ryanair 3
Ultimaco 3
Tom Online 3
RSA 3
Reservoir 3
RunKeeper 3
StateWatch 3
Doodle 3
PittPatt 3
Emotient 3
Zalando 3
DRIP-UK 3
Regin 3
ThomsonReuters 3
Tianji.com 3
Passbook 3
Lockitron 3
Kévo 3
LulzSec 3
Skywriter 3
Waterwitch 3
Stellar 3
Nightwatch 3
Marina 3
Tao 3
Perrier 3
Commission Nationale de Protection de Données - Maroc (CNDP ) 3
radar 3
Thomas Cook 3
Myfox 3
Myfox Around 3
Plateforme Nationale de Cryptage et de Décryptement (PNCD) 3
Zensorium 3
Jooxter 3
SecureChat 3
Stripe 3
Withings Pulses 3
Suprnova 3
Samsung SleepSense 3
Withings Aura 3
Realo 3
Mattel 3
FreedomHouse 3
Geofeedia 3
Knockin 3
FindFace 3
cerveau 3
OnePlus 3
Kunlun 3
DeepFake 3
NaMo 3
Broadcom 3
Oath 3
TestAchats 3
Ntechlab 3
Lime 3
The Spinner 3
Reverso 3
Ligue internationale contre le racisme et l'antisémitisme (Licra) 3
OpenAI 3
Nestor 3
LREM 3
Telstra 3
TutorABC 3
DarkMatter 3
Snap 3
SnapLion 3
iBorderCtrl 3
iFixit 3
Stasi 3
BSV 3
BellTroX 3
WatchNext 3
Boogaloo 3
JustWalkOut 3
Cerberus 3
DarkBasin 3
MoutonNumérique 3
Blackstone 3
Excel 3
Cerema 3
SafeTechAct 3
Clubhouse 3
MoveOn 2
vnunet 2
Yahoo Ad Interest Manager 2
Nikon 2
Liberty 2
photocopieuse 2
RecordedFuture 2
Visible Technologies 2
NetBook 2
NBC Universal 2
LockheedMartin 2
Traffic Marketplace 2
Mobclix 2
Millennial Media 2
TextPlus 2
Sunrise 2
Newsday 2
ABC 2
MTV 2
Specific Media 2
Scribt 2
InteractiveAdvertisingBureau-IAB 2
Human Rights Activists News Agency (HRANA) 2
Consumer Action 2
Privacy Rights Clearinghouse 2
Privacy Times 2
McAfee 2
Tagged 2
Unabhängige Landeszentrum für Datenschutz (ULD) 2
Usenet FTD 2
Ntrepid 2
Société de l'Industrie Musicale (SIMIM) 2
Streetside 2
MafiaaFire 2
Sociedad General de Autores y Editores (SGAE) 2
Pakistan Telecommunications Authority (PTA) 2
GooglePlaces 2
So.cl 2
Human Rights Law Foundation 2
FreedomofInformationAct 2
Service Canadien du Renseignement de Sécurité (SCRS) 2
C-30 (projet de loi) 2
Crédit Mutuel-CIC 2
Knowledge Network 2
MFG Labs 2
Xoom 2
Yahoo Voice 2
Ustream 2
Vobile 2
Webhelp 2
Fair Labor Association (FLA) 2
Orbitz 2
KT Corp 2
diffamation 2
Limited Run 2
Rapid Enforcement Allied Computer Team (REACT) 2
VoIP 2
Roland-Garros 2
BMG Rights Management 2
Tor Books 2
Symplified 2
Ping Identity 2
Okta 2
RCS Da Vinci 2
Phoenexia 2
joystick 2
R.A.T 2
Kyocera 2
Epson 2
Lexmark 2
Ricoh 2
Kee Square 2
Prosegur (PSG) 2
Wise Intelligent Technology (WIT) 2
Kyos 2
Smart Flows 2
Wanadoo 2
Quantum insert 2
Renew 2
poubelle intelligente 2
WordPress 2
Shell 2
Magnolia Pictures 2
Vine 2
Telfrance Serie 2
English PEN 2
Nymi 2
Proximus Security 2
Orpea-Clinea 2
ZunZuneo 2
Persistent Surveillance Systems (PSS) 2
Nike Plus 2
Maxymiser 2
Netra 2
Scylt 2
Yes Youth Can 2
NeoCities 2
USB 2
Spider.io 2
Next Generation Identification (NGI) 2
Banque Postale 2
Turkcell 2
TalktoPay-T2P 2
Surespot 2
Welcome 2
MyFitnessPal 2
Morpho Video Investigator (MVI) 2
Altern.org 2
Spallian 2
Omate 2
Safe Family 2
Kids Watch 2
KiLife 2
Kiband 2
paxie 2
Sync Smartband 2
Token 2
TUTO4PC 2
Shopify 2
Quantumbot 2
National Intelligence Service (NIS) - South Korea 2
StratoBus 2
Sp0n 2
Vigilante 2
Mirai 2
Sneakairs 2
EpicGames 2
Philips 2
Netatmo Présence 2
casque 2
Payoneer 2
Snapchat Map 2
MACadress 2
uGrow 2
QuintilesIMS 2
Unroll.Me 2
Affectiva 2
Rue du Commerce 2
National Geospatial-Intelligence Agency 2
Realface 2
GEO 2
Unity Biotechnology 2
Terrogence 2
Sandvine/Procera 2
Turk Telecom 2
ExodusPrivacy 2
Syndicat des Travailleurs du Jeu Vidéo (STJV) 2
Store Sales Measurement 2
Vectaury 2
Jump 2
Rosetta 2
Singlespot 2
Social network Analysis (SCN) 2
Mapbox 2
FacebookDating 2
Onavo 2
LocationSmart 2
Universal Human Relevance System (UHRS) - Microsoft 2
PersistentSurveillanceSystem 2
Wirk 2
Sogou 2
Pax 2
AmazonMarketPlace 2
Absher 2
Moulinsart 2
XuexiQiangguo 2
ShenzhenSmartCareTechnology 2
Taster 2
GoogleWorkoutforRealChange. 2
Youzan 2
BeAware 2
Wizzlabs 2
E7mi 2
Tatamman 2
Wiqaytna 2
Megvii 2
Jump 2
Lime 2
Billy 2
Sciensano 2
FamilyTreeDNA 2
AfricanAncestry 2
APIG 2
SwissCovid 2
DossierPénalNumérique-DPN 2
Jitsi 2
WeDoctor 2
Alihealth 2
DingDing 2
Genetec 2
Kustomer 2
Winners &amp; HomeSense 1
CRIOC 1
imprimante 1
Société Minière des Grands Lacs 1
Standard Life 1
Palm 1
Skyblog 1
inside google 1
Verbraucherzentrale Bundesverband (VZBV) 1
April 1
3PAR 1
CBS 1
Information Warfare Monitor 1
Skyhook Wireless 1
NTT 1
Samsung Apps 1
Skyfire 1
Revenu Québec 1
Twitter Sign In 1
Yahoo Identity 1
Procter&amp;Gamble 1
Mimesis Republic 1
Verein für Anti-Piraterie der österreichischen Film und Videobranche (VAP) 1
AntiSec 1
Rutracker 1
Voucherstar 1
Sega 1
Newzbin 1
Ozapftis 1
R2D2 1
British Recorded Music Industry (BRMI) 1
Publicis 1
Sony Reader Store 1
Nespresso 1
Nominet 1
Point de Contact 1
Lagardère 1
Simon &amp; Schuster 1
Penguin 1
Verlagsgruppe Georg von Holzbrinck 1
Data Protection Act 1
yatedo 1
Logica 1
Trivisio 1
Vibrant Media Inc. 1
WPP PLC's Media Innovation Group LLC 1
Perception Understanding Learning System Activity Recognition (Pulsar) 1
Wooga 1
NC Soft 1
Kontagent 1
PeakGames 1
Serval 1
shack.net 1
Mitsubishi 1
Marshall Cavendish 1
Proteus Digital Health 1
Syndicat des industries de matériels audiovisuels (Simavelec) 1
SFIB (technologies de l'information) 1
Secimavi (fabricants et importateurs d'appareils électroniques grand public) 1
Syndicat national des supports d'image et d'information (SNSII) 1
Steam 1
Windows Media Player 1
Kaleidescape 1
Takedown Piracy 1
Twenga 1
VirnetX 1
ReconRobotics 1
Scout XT 1
Mapmymobile 1
National Aeronautics and Space Administration (NASA) 1
Traffic Sniffing 1
Office Français de Protection des Réfugiés et Apatrides (OFPRA) 1
Steria 1
Teradata 1
Moto X 1
PirateBrowser 1
Secom 1
Policy Violation Checker 1
Rutor 1
Trax 1
MicroStrategy 1
Wisdom Professional 1
Keylemon 1
L-1 1
Zaarly 1
Nederlandse College bescherming persoonsgegevens (CBP) 1
Primatice 1
EE 1
TripAdviser 1
Spoutnik 1
Rambler 1
BSkyB 1
Safe Zone 1
Quividi 1
Rootkit 1
Keecker 1
TEC 1
DEA 1
Secret 1
YikYak 1
Kraft 1
X-UIDH 1
Retro 1
Sell Hack 1
Namo Media 1
Syndicat des Industries du Génie Numérique, Énergétique et Sécuritaire (IGNES) 1
Xtra Interim 1
Midi System 1
SGME 1
Radio Physics Solution 1
NeoFace 1
Three Days Rule 1
PRO BTP 1
Pathway Genomics 1
Garante per la Protezione dei Dati Personali 1
SmallWorld 1
Tafacalou 1
NBot 1
FranceTélévisions 1
Nethawk 1
Network Injector Appliance (NIA) - Hacking Team 1
Perceptio Media 1
Watch Being 1
Mobistar 1
Poppy 1
Whirpool 1
Weather Channel 1
Service Central de Renseignement Criminel (SCRC) - France 1
Project Oxford 1
Barbie 1
Shutterfly 1
Take-Two Interactive Software 1
Norwegian Consumer Council (NCC) 1
Revolv 1
Telus 1
Faception 1
Government Account Office (GAO) / USA 1
Semlex 1
Sodexo 1
Swipebuster 1
Standard Innovation 1
We-vibe 1
Shiseido 1
Shanghai Adups Technology Company 1
Keyhole 1
Métrobus 1
Posterscope 1
Vigilant 1
SharpMask 1
Meitu 1
OxygenOS 1
TV5 1
National Cable and Telecommunications Association (NCTA) 1
Urssaf 1
Campaign for Accountability (USA) 1
Walgreens 1
InternetResearchAgency-IRA 1
MiTu 1
Xiao Tian Cai 1
myPersonality 1
Optimizely 1
pacemaker 1
LLVision 1
Temasek 1
Roche 1
CarrotInsights 1
CarrotRewards 1
DataPropria 1
Spil.ly 1
Neuromation 1
Pallas 1
Toutiao 1
Swann Security 1
Ripley 1
SfootBD 1
Spyfone 1
Neuralink 1
My Hammer 1
exosquelette 1
MegaFace 1
Social Sentinel 1
HeritageFoundation 1
Avaaz 1
Ascension 1
Sécurité sociale 1
Riyuk 1
Liga (SP) 1
PizzaChecker 1
Vumacam 1
The Shift Project 1
ThermoFischer 1
Aegis 1
Sierra Nevada Corporation 1
Secure Justice 1
Change.org 1
Éthique sur l'etiquette 1
Wagner 1
Preligens 1
RedBull 1
WWF 1
Glonass 1
NaviC 1
Beidou 1
QZSS 1
Sabre 1
Wistron 1
Dharma 1
Mobilewalla 1
Perceptics 1
YY 1
SuperCom 1
FacebookShop 1
TheCollective 1
VoteMAP 1
Civvl 1
FaceAPI 1
OneSearch 1
Dahua 1
DigitalServicesAct 1
BIPA 1
