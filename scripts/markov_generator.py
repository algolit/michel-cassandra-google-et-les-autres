import json
from markov import Markov
import random
import re
import os.path

keyword = 'biometrics'
language = 'en'

data = json.load(open(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data', '{}.json'.format(language))))

if language == 'fr':
	from pattern.fr import parsetree
elif language == 'en':
	from pattern.en import parsetree
else:
	from pattern.nl import parsetree

corpus = []


"""
{
	'fr': {
		'2007': [
			{
				'id': 'id',
				'title': 'title',
				'date': 'date',
				'keywords': {
					'company': [],
					'web': [],
					'tools': [],
					'subject': [],
					'citizen_protection': [],
				},
				'zone': [],
				'type': []
				'text': 'text',
				'source': (title, url)
			},
		]
	},
}
"""

# Filter function to test if the article has a given keyword
def has_keyword_filter (article):
  return keyword in article['keywords']['subject']

# Filters for articles have the given keyword
for year, articles in data[language].items():
  corpus.extend(filter(has_keyword_filter, articles))


markov_generator = Markov(window=3)

random.shuffle(corpus)

# Train on first 250 articles
for article in corpus[:250]:
	print(article['title'], 'with length:', len(article['text']))
	parsed_text = parsetree(article['text'], tokenize=True, tags=False, chunks=False, relations=False, lemmata=False)
	# tokens = [[word.string for word in sentence.words] for sentence in parsed_text.sentences]
	tokens = []

	for sentence in parsed_text.sentences:
		tokens.extend([word.string for word in sentence.words])

	markov_generator.read(tokens)

# Transforms list of tokens back to a string. Should be slightly intelligent.
def make_text (words):
  text = ''
  first_word = True
  captilize = True

  for word in words:
    if re.match(r'\W', word) is not None:
      text += word

      if re.match(r'[!?\.]+$', word) is not None:
        captilize = True

      if re.match(r'\n+$', word) is not None:
        first_word = True

    else:
      if not first_word:
        text += ' '

      text += word.capitalize() if captilize else word
      captilize = False
      first_word = False

  return text

print(make_text(markov_generator.generate(250)))

# print("Reading text file", flush=True)
# with open('input/cat.txt', 'r') as h:
#   # Read text file
#   raw_text = h.read()

#   print("Tokenizing", flush=True)
  

#   print("\n*****")
#   print("Making model (bigram)", flush=True)
#   model = make_markov_model(tokens, history = 2)
#   print(get_model_statistics(model))
  
#   print("Generating text", flush=True)
#   print(make_text(generate(model, seed=["the", "cat"])))
