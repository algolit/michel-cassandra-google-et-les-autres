from collections import Counter
import json
import csv
import os.path

def make_keyword_counters ():
  return {
    'company': Counter(),
    'web': Counter(),
    'tools': Counter(),
    'subject': Counter(),
    'citizen_protection': Counter()
  }

for language in ['en', 'fr', 'nl']:
  data = json.load(open(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data', '{}.json'.format(language))))
  
  keyword_counter_overall = make_keyword_counters()
  keyword_counters_year = {}
  article_count_per_year = {}
  article_counter = 0
  
  for year, articles in data[language].items():
    article_count_per_year[year] = len(articles)
    article_counter += len(articles)
    
    if year not in keyword_counters_year:
      keyword_counters_year[year] = make_keyword_counters()

    for article in articles:
      for category in article['keywords']:
        category_keywords = [k.lower() for k in article['keywords'][category]]

        if category_keywords:
          keyword_counters_year[year][category].update(category_keywords)
          keyword_counter_overall[category].update(category_keywords)

  print(article_count_per_year)

  # Output a sheet per category:
  #           2017    2018  2019
  # word 1    freq
  # word 2    freq
  # word 3    freq

  # loop through the categories
  #   transform the counters of the category into a list of items and frequencies 

  for category in keyword_counter_overall.keys():
    words, _ = zip(*keyword_counter_overall[category].most_common())
    
    with open('keyword-frequencies-relative-{}-{}.csv'.format(category, language), 'w') as csvfile:    
      writer = csv.writer(csvfile)

      # Add column names to the sheet
      headers = ['word'] + sorted(keyword_counters_year.keys())
      writer.writerow(headers)

      # Loop through the words and lookup frequencies per year
      for word in words:
        row = [word]

        for year in sorted(keyword_counters_year.keys()):
          year_frequencies = row.append(keyword_counters_year[year][category][word] / article_count_per_year[year])

        writer.writerow(row)