import json

import random
import re
import os.path

from utils import get_last_word

keyword = 'gigeconomy'
category = 'subject'
language = 'en'


def write_poem (keyword, category, language, max_sentence_length=12):
  data = json.load(open(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data', '{}.json'.format(language))))

  if language == 'fr':
    from pattern.fr import parsetree
    from rhymes_fr import rhymes, count_syllables
  elif language == 'en':
    from pattern.en import parsetree
    from rhymes_en import rhymes, count_syllables
  else:
    from pattern.nl import parsetree

  """
  {
    'fr': {
      '2007': [
        {
          'id': 'id',
          'title': 'title',
          'date': 'date',
          'keywords': {
            'company': [],
            'web': [],
            'tools': [],
            'subject': [],
            'citizen_protection': [],
          },
          'zone': [],
          'type': []
          'text': 'text',
          'source': (title, url)
        },
      ]
    },
  }
  """

  def has_keyword_filter (keyword, category):
    def filter (article):
      return keyword in list(map(str.lower, article['keywords'][category]))

    return filter

  def count_sentence_syllables (sentence):
    count = 0

    for word in sentence.words:
      count += count_syllables(word.string)

    return count

  sentences = []
  syllable_index = {}
  last_word_index = {}
  last_word_syllable_index = {}

  # Loop through data and construct indexes
  for year in sorted(data[language].keys()):
    print(year)
    articles = list(filter(has_keyword_filter(keyword, category), data[language][year]))
    print(len(articles))
    if len(articles) > 0:
      # Set article list in random order, as not all of them ar processed
      random.shuffle(articles)

      for article in articles:
        parsed_text = parsetree(article['text'], tokenize=True, tags=False, chunks=False, relations=False, lemmata=False)
        
        for sentence in parsed_text.sentences:
          if len(sentence.words) < max_sentence_length:

            last_word = get_last_word(sentence).lower()
            syllable_count = count_sentence_syllables(sentence)

            sentences.append((sentence.string, last_word, syllable_count))

            if last_word not in last_word_index:
              last_word_index[last_word] = []

            last_word_index[last_word].append(len(sentences)-1)

            if syllable_count not in syllable_index:
              syllable_index[syllable_count] = []

            syllable_index[syllable_count].append(len(sentences)-1)

            if (last_word.lower(), syllable_count) not in last_word_syllable_index:
              last_word_syllable_index[(last_word.lower(), syllable_count)] = []
            
            last_word_syllable_index[(last_word.lower(), syllable_count)].append(len(sentences)-1)

  def find_rhyming_pair (max_attempts = 100, desired_syllable_count=None):
    sentence = None
    syllable_count = None
    rhyming_sentence = None
    rhyming_syllable_count = None
    attempts = 0

    while not rhyming_sentence and attempts < max_attempts:
      attempts += 1
      last_word = None
      if desired_syllable_count:
        if desired_syllable_count in syllable_index:
          options = syllable_index[syllable_count-1] + syllable_index[syllable_count] + syllable_index[syllable_count+1]
          sentence, last_word, syllable_count = sentences[random.choice(options)]
      else:
        sentence, last_word, syllable_count = random.choice(sentences)

      if last_word:
        rhyme_words = rhymes(last_word.lower())
        random.shuffle(rhyme_words)

        while not rhyming_sentence and rhyme_words:
          rhyme_word = rhyme_words.pop(0).lower()

          if (rhyme_word, syllable_count) in last_word_syllable_index:
            rhyming_sentence, _, rhyming_syllable_count = sentences[random.choice(last_word_syllable_index[rhyme_word, syllable_count])]

    return ((sentence, syllable_count), (rhyming_sentence, rhyming_syllable_count))

  results = []

  for _ in range(25):
    a = find_rhyming_pair()
    b = find_rhyming_pair(a[0][1])

    results.append([
      a[0][0],
      b[0][0],
      b[1][0],
      a[1][0]
    ]) 

  return results

if __name__ == '__main__':
  import sys

  # Call with rhyming_sentences.py keyword category language

  results = write_poem(sys.argv[1], sys.argv[2], sys.argv[3])

  for poem in results:
    print(poem[0])
    print(poem[1])
    print(poem[2])
    print(poem[3])
    print()
    print()