import os.path

ETRACES_DATA_FOLDER = os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', 'data')

LANGUAGE = 'en'
MAX_SENTENCE_LENGTH = 13
MIN_SENTENCES = 500

BASEURL = ''