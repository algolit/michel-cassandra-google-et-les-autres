from sentence_utils import find_keyword, find_language, find_rhyme, find_sentence, random_keyword
from db import db_session

"""
  The collected articles have been split into sentences and stored in the database.
  The function `find_sentence` allows to find sentences in the database.
  The line below selects a random sentence.
  And prints its id and content
"""
sentence = find_sentence(db_session)
print(sentence.id)
print(sentence.text)

""""
  Almost all objects in the database are linked to a language.
  In the lines below first a language is selected then the language is used
  as a filter in the sentence selection.
"""
language = find_language(db_session, 'fr')
print(language.id)
sentence = find_sentence(db_session, language=language)
print(sentence.id)
print(sentence.text)
print() # newlines for readability.
print()

"""
  Another useful filter is the word count
"""
sentence = find_sentence(db_session, word_count=12, language=language)
print(sentence.id)
print(sentence.text)
print() # newlines for readability.
print()

"""
  For each sentence the last word is stored in the column `last_word`
  The id of the rhyming sound is stored on the column `last_word_rhyme_id`

  The function `find_sentence` allows the filter the found sentence directly
  by rhyme sound. In the example below two sentences are selected with five words
  ending in the rhyming part 'it', from 'algolit'
"""
print("Sentences rhyming with 'algolit', word count 5")
print(find_sentence(db_session, rhyme='it', word_count=5, language=language).text)
print(find_sentence(db_session, rhyme='it', word_count=5, language=language).text)
print() # newlines for readability.
print()


"""
  Filtering again on the rhyming part 'it' but this time using the field
  `syllable_count`
"""
print("Sentences rhyming with 'algolit', syllable count 10")
print(find_sentence(db_session, rhyme='it', syllable_count=10, language=language).text)
print(find_sentence(db_session, rhyme='it', syllable_count=10, language=language).text)
print() # newlines for readability.
print()

"""
  The function `rhyming_part` found in `rhymes_fr` returns the rhyming
  part for words included in the Lexique lexicon.

  In the lines below the function is imported and two sentences are selected
  which rhyme with ordinateur.

  As the rhyming and / or phoneme notation is language dependent, rhymes
  extracted with the french rhyming_part function will return french sentences.
  And rhymes extracted with the english rhyming_part function will return english
  sentences.
"""

from rhymes_fr import rhyming_part as rhyming_part_fr

rhyme_ordinateur = rhyming_part_fr('ordinateur')
print("Sentences rhyming with 'ordinateur'")
print(find_sentence(db_session, rhyme=rhyme_ordinateur, syllable_count=10, language=language).text)
print(find_sentence(db_session, rhyme=rhyme_ordinateur, syllable_count=10, language=language).text)
print() # newlines for readability.
print()

# keyword = random_keyword(db_session)
# keyword = find_keyword(db_session, 'biométrie')
# print(keyword.keyword, keyword.id)

# sentence = find_sentence(db_session, keyword=keyword)
# print(sentence.text)