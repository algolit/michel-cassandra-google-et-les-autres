import datetime
import re
import time
from collections import defaultdict, OrderedDict
from operator import itemgetter 
from nltk.corpus import stopwords
import csv


# IMPORTING ARTICLES BY title, date, keywords, text source

path = 'analyse.txt'

def getTitle (line):
	m = re.match(r'^-- (.+)$', line)
	if m:
		return m.group(1)
	return None

# Returns date object
def getDate (line):
	m  = re.match(r'(\d{,2})/(\d{,2})/(\d{4})', line)
	if m:
		return datetime.date(int(m.group(3)), int(m.group(2)), int(m.group(1)))

# Returns list of keywords
def getKeywords (line):
	keywords = re.split(r'\+\+\s', line)
	return [k.strip() for k in keywords if k]

# Returns tuple (sourcename, sourceurl)
def getSource (line):
	m = re.match('source: (.+) \+ (.+)', line, re.I)
	if m:
		return (m.group(1), m.group(2))
	pass

articles = []

with open(path, 'r') as h:
	lines = h.readlines()
	linecount = len(lines)
  
	for o in range(0, linecount, 10):
		if linecount - o >= 8:
			articles.append({
				'title': getTitle(lines[o]),
				'date': getDate(lines[o + 2]),
				'keywords': getKeywords(lines[o + 4]),
				'text': lines[o + 6],
				'source': getSource(lines[o + 8])
			}
	)
