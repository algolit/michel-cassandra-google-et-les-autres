from pronouncing import rhymes, phones_for_word, syllable_count
import math

import pronouncing

def rhyming_part (word):
  pronunciations = pronouncing.phones_for_word(word)
  if len(pronunciations) > 0:
    return pronouncing.rhyming_part(pronunciations[0])
  else:
    return ''
    
def count_syllables(word):
  phones = phones_for_word(word)
        
  if phones:
    return syllable_count(phones[0])
  else:
    return math.floor(len(word) / 2.5)
