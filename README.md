# Michel Cassandra Google et Les Autres

Anaylizing Michel Cleempoel's e-traces database.

## Requirements

For the scripts to work [pronouncing](https://pronouncing.readthedocs.io/en/latest/) and [pattern](https://github.com/clips/pattern/wiki) are required. For the web interface Flask is required too.
Easiest is to install them through pip:

```
pip install -r scripts/requirements.txt
```

To derive rhyming and syllable counts Lexique is used. Download it here: <http://www.lexique.org/databases/Lexique383/Lexique383.zip>. And save it in `scripts/data`.

## Data

Download the data and place it in the folder `scripts/data`.


## Scripts

`scripts/count-keywords.py` && `scripts/count-keywords.py` count keywords in the data and generate csv files.

`scripts/markov_generator.py` generates sentence using a markov chain, based on a given keyword.

`scripts/markov_year_generator.py` generates a sentences for every year between 2007 & 2021 with articles using a given keyword.

`scripts/markov_rhyme_generator.py` generates two sentences, the second should rhyme with the first, while having the same amount of syllables. Based on articles using a set keyword.


## Poem generator / extracting rhyming sentences

The generator constructs poems out of two sets of rhyming sentences. These sentences are linked to articles which use a given keyword.

`scripts/commandline_poem.py` commandline interface for the poem generator
`scripts/app.py` web interface to the poem generator
`scripts/run.sh` launches the web interface

`scripts/poem.py` functions to actually generate the poems

`scripts/count-eligible-poem-sentences.py` counts eligible sentences per keyword

## Used libraries
- [pattern](https://github.com/clips/pattern/wiki/)
- [pronouncing](https://pronouncing.readthedocs.io/en/latest/)