
languages = [
  'de',
  'en',
  'es',
  'fr',
  'it',
  'nl',
  'pt_br'
]

for lang in languages:
    for year in range(2007,2022):
      print('wget "https://etraces.constantvzw.org/informations/spip.php?page=export_agolight&lang={lang}&quand={year}-01-01" -O {lang}-{year}.txt'.format(year=year, lang=lang))